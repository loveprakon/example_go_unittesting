package entity

type Customer struct {
	IDCard    string
	FirstName string
	LastName  string
}
