package usecase

import "errors"

var (
	ErrCustomerNotFound = errors.New("customer not found")
	ErrCustomerExist    = errors.New("customer exist")
)
