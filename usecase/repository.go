package usecase

import "gitlab.com/loveprakon/example_go_unittesting/entity"

type CustomerInterface interface {
	Save(id string, firstName string, LastName string) error
	FindOneWithIDCard(id string) (entity.Customer, error)
}
