package usecase

func NewCustomerUseCase(customerRepo CustomerInterface) CustomerUseCase {
	return CustomerUseCase{
		customerRepository: customerRepo,
	}
}

type CustomerUseCase struct {
	customerRepository CustomerInterface
}

type CreateCustomerInput struct {
	IDCard    string
	FirstName string
	LastName  string
}

func (u CustomerUseCase) CreateCustomer(input CreateCustomerInput) error {

	_, err := u.customerRepository.FindOneWithIDCard(input.IDCard)
	if err != ErrCustomerNotFound {
		if err == nil {
			return ErrCustomerExist
		}
		return err
	}

	err = u.customerRepository.Save(input.IDCard, input.FirstName, input.LastName)
	if err != nil {
		return err
	}
	return nil
}
