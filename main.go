package main

import (
	"context"
	"fmt"
	"gitlab.com/loveprakon/example_go_unittesting/repository/customer"
	"gitlab.com/loveprakon/example_go_unittesting/usecase"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	cusRepo := initRepository()
	uc := usecase.NewCustomerUseCase(cusRepo)

	input := usecase.CreateCustomerInput{
		IDCard:    "123456",
		FirstName: "fname",
		LastName:  "lname",
	}
	if err := uc.CreateCustomer(input); err != nil {
		fmt.Println("err:", err)
	}

}

func initRepository() usecase.CustomerInterface {
	uri := "mongodb://root:example@localhost:27017/?authsource=admin"
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(uri))
	if err != nil {
		panic(err)
	}
	db := client.Database("example")
	cusRepo := customer.NewMongoDb(db)
	return cusRepo
}
