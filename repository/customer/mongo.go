package customer

import (
	"context"
	"gitlab.com/loveprakon/example_go_unittesting/entity"
	"gitlab.com/loveprakon/example_go_unittesting/usecase"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func NewMongoDb(db *mongo.Database) usecase.CustomerInterface {
	coll := db.Collection("customer")
	return mongoDb{
		coll: coll,
	}
}

type mongoDb struct {
	coll *mongo.Collection
}

type customer struct {
	IDCard    string `bson:"idCard"`
	FirstName string `bson:"firstName"`
	LastName  string `bson:"lastName"`
}

func (m mongoDb) Save(id string, firstName string, LastName string) error {
	newCustomer := customer{
		IDCard:    id,
		FirstName: firstName,
		LastName:  LastName,
	}
	if _, err := m.coll.InsertOne(context.TODO(), newCustomer); err != nil {
		return err
	}
	return nil
}

func (m mongoDb) FindOneWithIDCard(id string) (entity.Customer, error) {
	var zero entity.Customer
	filter := bson.D{{"idCard", id}}
	var doc customer
	err := m.coll.FindOne(context.TODO(), filter).Decode(&doc)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return zero, usecase.ErrCustomerNotFound
		}
		return zero, err
	}
	return entity.Customer{
		IDCard:    doc.IDCard,
		FirstName: doc.FirstName,
		LastName:  doc.LastName,
	}, nil
}
